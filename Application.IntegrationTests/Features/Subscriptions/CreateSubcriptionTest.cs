﻿using BookStore.Application.Features.Subscriptions.Commands.CreateSubcription;
using BookStore.Domain.Entities;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Features.Subscriptions
{
    using static Testing;
    public class CreateSubcriptionTest : TestBase
    {
        [Test]
        public void ShouldRequireMinimumFields()
        {
            var command = new CreateSubscriptionCommand();

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().ThrowAsync<ValidationException>();
        }

        [Test]
        public async Task ShouldCreateSubscription()
        {
            var userId = await RunAsDefaultUserAsync();

            var command = new CreateSubscriptionCommand
            {
                DateFrom = DateTime.Now,
                DateTo = DateTime.Now.AddDays(30),
                Discount = 0M,
                TotalPrice = 100.00M,
                UserId = userId
            };

            var itemId = await SendAsync(command);

            var item = await FindAsync<Subscription>(itemId.Data);

            item.Should().NotBeNull();
            item.DateFrom.Should().Be(command.DateFrom);
            item.DateTo.Should().Be(command.DateTo);
            item.Discount.Should().Be(command.Discount);
            item.TotalPrice.Should().Be(command.TotalPrice);
            item.UserId.Should().Be(command.UserId);
            item.CreatedBy.Should().Be(userId);
            //item.Created.Should().BeCloseTo(DateTime.Now, 0.5.Seconds());
            item.LastModifiedBy.Should().BeNull();
            item.LastModified.Should().BeNull();
        }
    }
}
