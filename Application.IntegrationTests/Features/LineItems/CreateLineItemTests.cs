﻿using BookStore.Application.Features.Books.Commands.CreateBook;
using BookStore.Application.Features.LineItems.Commands.CreateLineItem;
using BookStore.Application.Features.Subscriptions.Commands.CreateSubcription;
using BookStore.Domain.Entities;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Features.LineItems
{
    using static Testing;
    public class CreateLineItemTests : TestBase
    {
        [Test]
        public void ShouldRequireMinimumFields()
        {
            var command = new CreateLineItemCommand();

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().ThrowAsync<ValidationException>();
        }

        [Test]
        public async Task ShouldCreateLineItem()
        {
            var userId = await RunAsDefaultUserAsync();

            var subCommand = new CreateSubscriptionCommand
            {
                DateFrom = DateTime.Now,
                DateTo = DateTime.Now.AddDays(30),
                Discount = 0M,
                TotalPrice = 100.00M,
                UserId = userId
            };
            var subscriptionId = await SendAsync(subCommand);

            var bookCommand = new CreateBookCommand
            {
                Name = "Name",
                Description = "Book description",
                Barcode = "12345-ABC",
                PurchasePrice = 100.00M
            };

            var bookId = await SendAsync(bookCommand);

            var command = new CreateLineItemCommand
            {
                BookId = bookId.Data,
                SubscriptionId = subscriptionId.Data,
                Quantity = 1,
                TotalAmount = 100.00M
            };


            var itemId = await SendAsync(command);

            var item = await FindAsync<LineItem>(itemId.Data);

            item.Should().NotBeNull();
            item.BookId.Should().Be(command.BookId);
            item.SubscriptionId.Should().Be(command.SubscriptionId);
            item.Quantity.Should().Be(command.Quantity);
            item.TotalAmount.Should().Be(command.TotalAmount);
            item.CreatedBy.Should().Be(userId);
            //item.Created.Should().BeCloseTo(DateTime.Now, 0.5.Seconds());
            item.LastModifiedBy.Should().BeNull();
            item.LastModified.Should().BeNull();
        }
    }
}
