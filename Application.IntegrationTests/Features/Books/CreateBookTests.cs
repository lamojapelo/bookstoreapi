﻿using BookStore.Application.Features.Books.Commands.CreateBook;
using BookStore.Domain.Entities;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.IntegrationTests.Features.Books
{
    using static Testing;
    public class CreateBookTests : TestBase
    {
        [Test]
        public void ShouldRequireMinimumFields()
        {
            var command = new CreateBookCommand();

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().ThrowAsync<ValidationException>();
        }

        [Test]
        public async Task ShouldCreateBook()
        {
            var userId = await RunAsDefaultUserAsync();

            var command = new CreateBookCommand
            {
                Name = "Name",
                Description = "Book description",
                Barcode = "12345-ABC",
                PurchasePrice = 100.00M
            };

            var itemId = await SendAsync(command);

            var item = await FindAsync<Book>(itemId.Data);

            item.Should().NotBeNull();
            item.Name.Should().Be(command.Name);
            item.Description.Should().Be(command.Description);
            item.Barcode.Should().Be(command.Barcode);
            item.PurchasePrice.Should().Be(command.PurchasePrice);
            item.CreatedBy.Should().Be(userId);
            //item.Created.Should().BeCloseTo(DateTime.Now, 0.5.Seconds());
            item.LastModifiedBy.Should().BeNull();
            item.LastModified.Should().BeNull();
        }
    }
}
