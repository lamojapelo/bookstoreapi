﻿using BookStore.Application.Interfaces.Repositories;
using BookStore.Domain.Entities;
using BookStore.Infrastructure.Persistence.Contexts;
using BookStore.Infrastructure.Persistence.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class SubscriptionRepositoryAsync : GenericRepositoryAsync<Subscription>, ISubscriptionRepositoryAsync
    {
        private readonly DbSet<Subscription> _subscriptions;

        public SubscriptionRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _subscriptions = dbContext.Set<Subscription>();
        }
    }
}
