﻿using BookStore.Application.Interfaces.Repositories;
using BookStore.Domain.Entities;
using BookStore.Infrastructure.Persistence.Contexts;
using BookStore.Infrastructure.Persistence.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class LineItemRepositoryAsync : GenericRepositoryAsync<LineItem>, ILineItemRepositoryAsync
    {
        private readonly DbSet<LineItem> _lineItems;
        public LineItemRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _lineItems = dbContext.Set<LineItem>();
        }
    }
}
