﻿using BookStore.Application.Interfaces.Repositories;
using BookStore.Domain.Entities;
using BookStore.Infrastructure.Persistence.Contexts;
using BookStore.Infrastructure.Persistence.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Infrastructure.Persistence.Repositories
{
    public class BasketRepositoryAsync : GenericRepositoryAsync<BasketItem>, IBasketRepositoryAsync
    {
        private readonly DbSet<BasketItem> _basketItems;
        public BasketRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _basketItems = dbContext.Set<BasketItem>();
        }
        
    }
}
