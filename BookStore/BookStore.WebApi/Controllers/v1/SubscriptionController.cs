﻿using BookStore.Application.Features.Subscriptions.Commands.CreateSubcription;
using BookStore.Application.Filters;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.WebApi.Controllers.v1
{
    public class SubscriptionController : BaseApiController
    {
        //[HttpGet]
        //public async Task<IActionResult> Get([FromQuery] RequestParameter filter)
        //{
        //    return Ok(await Mediator.Send(new GetAllSubscriptionsQuery() { PageSize = filter.PageSize, PageNumber = filter.PageNumber }));
        //}

        // POST api/<controller>
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post(CreateSubscriptionCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
