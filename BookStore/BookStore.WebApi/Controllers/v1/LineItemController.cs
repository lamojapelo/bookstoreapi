﻿using BookStore.Application.Features.LineItems.Commands.CreateLineItem;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.WebApi.Controllers.v1
{
    public class LineItemController : BaseApiController
    {
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post(CreateLineItemCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
