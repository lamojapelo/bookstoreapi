﻿using BookStore.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Domain.Entities
{
    public class LineItem : AuditableBaseEntity
    {
        public int BookId { get; set; }
        public int SubscriptionId { get; set; }
        public int Quantity { get; set; }
        public decimal TotalAmount { get; set; }
        public Book Book { get; set; }
        public Subscription Subscription { get; set; }
    }
}
