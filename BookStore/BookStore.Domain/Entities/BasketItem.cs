﻿using BookStore.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Domain.Entities
{
    public class BasketItem : AuditableBaseEntity
    {
        public BasketItem()
        {
            Book = new Book();
        }
      
        public int BookId { get; set; }
        public virtual Book Book { get; set; }
        public int Quantity { get; set; }
        public string UserId { get; set; }
    }
}
