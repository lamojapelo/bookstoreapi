﻿using BookStore.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Domain.Entities
{
    public class Subscription : AuditableBaseEntity
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalPrice { get; set; }
        public string UserId { get; set; }
        public virtual ICollection<LineItem> LineItems { get; set; }

    }
}
