﻿using AutoMapper;
using BookStore.Application.Features.Books.Commands.CreateBook;
using BookStore.Application.Features.Books.Queries.GetAllBooks;
using BookStore.Domain.Entities;

namespace BookStore.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<Book, BookViewModel>().ReverseMap();
            CreateMap<CreateBookCommand, Book>();
            CreateMap<GetAllBooksQuery, GetAllBooksParameter>();
        }
    }
}
