﻿using AutoMapper;
using BookStore.Application.Wrappers;
using MediatR;
using BookStore.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;
using BookStore.Application.Interfaces.Repositories;
using System;
using Mapster;

namespace BookStore.Application.Features.Subscriptions.Commands.CreateSubcription
{
    public class CreateSubscriptionCommand : IRequest<Response<int>>
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalPrice { get; set; }
        public string UserId { get; set; }
        public class CreateSubscriptionCommandHandler : IRequestHandler<CreateSubscriptionCommand, Response<int>>
        {
            private readonly ISubscriptionRepositoryAsync _subscriptionRepository;
            //private readonly IMapper _mapper;
            public CreateSubscriptionCommandHandler(ISubscriptionRepositoryAsync subcriptionRepository, IMapper mapper)
            {
                _subscriptionRepository = subcriptionRepository;
                //_mapper = mapper;
            }

            public async Task<Response<int>> Handle(CreateSubscriptionCommand request, CancellationToken cancellationToken)
            {
                var subcription = request.Adapt<Subscription>();
                await _subscriptionRepository.AddAsync(subcription);
                return new Response<int>(subcription.Id);
            }
        }
    }
}
