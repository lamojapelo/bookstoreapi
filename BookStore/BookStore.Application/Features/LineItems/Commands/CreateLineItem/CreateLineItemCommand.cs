﻿using AutoMapper;
using BookStore.Application.Interfaces.Repositories;
using BookStore.Application.Wrappers;
using BookStore.Domain.Entities;
using Mapster;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BookStore.Application.Features.LineItems.Commands.CreateLineItem
{
    public class CreateLineItemCommand : IRequest<Response<int>>
    {
        public int BookId { get; set; }
        public int SubscriptionId { get; set; }
        public int Quantity { get; set; }
        public decimal TotalAmount { get; set; }
        public class CreateLineItemCommandHandler : IRequestHandler<CreateLineItemCommand, Response<int>>
        {
            private readonly ILineItemRepositoryAsync _lineItemRepository;
            private readonly IMapper _mapper;
            public CreateLineItemCommandHandler(ILineItemRepositoryAsync lineItemRepository, IMapper mapper)
            {
                _lineItemRepository = lineItemRepository;
                _mapper = mapper;
            }

            public async Task<Response<int>> Handle(CreateLineItemCommand request, CancellationToken cancellationToken)
            {
                var lineItem = request.Adapt<LineItem>();
                await _lineItemRepository.AddAsync(lineItem);
                return new Response<int>(lineItem.Id);
            }
        }
    }
}
