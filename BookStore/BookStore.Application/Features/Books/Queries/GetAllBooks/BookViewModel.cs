﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Application.Features.Books.Queries.GetAllBooks
{
    public class BookViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Barcode { get; set; }
        public string Description { get; set; }
        public decimal PurchasePrice { get; set; }
        public string ImageUrl { get; set; }
    }
}
