﻿using AutoMapper;
using BookStore.Application.Interfaces.Repositories;
using BookStore.Application.Wrappers;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace BookStore.Application.Features.Books.Queries.GetAllBooks
{
    public class GetAllBooksQuery : IRequest<PagedResponse<IEnumerable<BookViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllProductsQueryHandler : IRequestHandler<GetAllBooksQuery, PagedResponse<IEnumerable<BookViewModel>>>
    {
        private readonly IBookRepositoryAsync _bookRepository;
        private readonly IMapper _mapper;
        public GetAllProductsQueryHandler(IBookRepositoryAsync bookRepository, IMapper mapper)
        {
            _bookRepository = bookRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<BookViewModel>>> Handle(GetAllBooksQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllBooksParameter>(request);
            var book = await _bookRepository.GetPagedReponseAsync(validFilter.PageNumber, validFilter.PageSize);
            var bookViewModel = _mapper.Map<ICollection<BookViewModel>>(book);
            return new PagedResponse<IEnumerable<BookViewModel>>(bookViewModel, validFilter.PageNumber, validFilter.PageSize, bookViewModel.Count);
        }
    }
}
