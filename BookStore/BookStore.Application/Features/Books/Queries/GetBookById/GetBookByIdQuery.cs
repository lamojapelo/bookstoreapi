﻿using AutoMapper;
using BookStore.Application.Exceptions;
using BookStore.Application.Features.Books.Queries.GetAllBooks;
using BookStore.Application.Interfaces.Repositories;
using BookStore.Application.Wrappers;
using BookStore.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace BookStore.Application.Features.Books.Queries.GetBookById
{
    public class GetBookByIdQuery : IRequest<Response<BookViewModel>>
    {
        public int Id { get; set; }
        public class GetBookByIdQueryHandler : IRequestHandler<GetBookByIdQuery, Response<BookViewModel>>
        {
            private readonly IBookRepositoryAsync _bookRepository;
            private readonly IMapper _mapper;
            public GetBookByIdQueryHandler(IBookRepositoryAsync productRepository, IMapper mapper)
            {
                _bookRepository = productRepository;
                _mapper = mapper;
            }
            public async Task<Response<BookViewModel>> Handle(GetBookByIdQuery query, CancellationToken cancellationToken)
            {
                var book = await _bookRepository.GetByIdAsync(query.Id);
                if (book == null) throw new ApiException($"Book Not Found.");
                var bookViewModel = _mapper.Map<BookViewModel>(book);
                return new Response<BookViewModel>(bookViewModel);
            }
        }
    }
}
