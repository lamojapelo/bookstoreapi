﻿using BookStore.Application.Exceptions;
using BookStore.Application.Interfaces.Repositories;
using BookStore.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BookStore.Application.Features.Books.Commands.DeleteBookById
{
    public class DeleteBookByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteProductByIdCommandHandler : IRequestHandler<DeleteBookByIdCommand, Response<int>>
        {
            private readonly IBookRepositoryAsync _bookRepository;
            public DeleteProductByIdCommandHandler(IBookRepositoryAsync bookRepository)
            {
                _bookRepository = bookRepository;
            }
            public async Task<Response<int>> Handle(DeleteBookByIdCommand command, CancellationToken cancellationToken)
            {
                var product = await _bookRepository.GetByIdAsync(command.Id);
                if (product == null) throw new ApiException($"Product Not Found.");
                await _bookRepository.DeleteAsync(product);
                return new Response<int>(product.Id);
            }
        }
    }
}
