﻿using BookStore.Application.Exceptions;
using BookStore.Application.Interfaces.Repositories;
using BookStore.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BookStore.Application.Features.Books.Commands.UpdateProduct
{
    public class UpdateBookCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal PurchasePrice { get; set; }
        public class UpdateProductCommandHandler : IRequestHandler<UpdateBookCommand, Response<int>>
        {
            private readonly IBookRepositoryAsync _bookRepository;
            public UpdateProductCommandHandler(IBookRepositoryAsync productRepository)
            {
                _bookRepository = productRepository;
            }
            public async Task<Response<int>> Handle(UpdateBookCommand command, CancellationToken cancellationToken)
            {
                var book = await _bookRepository.GetByIdAsync(command.Id);

                if (book == null)
                {
                    throw new ApiException($"Book Not Found.");
                }
                else
                {
                    book.Name = command.Name;
                    book.PurchasePrice = command.PurchasePrice;
                    book.Description = command.Description;
                    await _bookRepository.UpdateAsync(book);
                    return new Response<int>(book.Id);
                }
            }
        }
    }
}
