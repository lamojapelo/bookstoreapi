﻿using BookStore.Application.Features.Books.Commands.CreateBook;
using BookStore.Application.Interfaces.Repositories;
using BookStore.Domain.Entities;
using FluentValidation;
using System.Threading;
using System.Threading.Tasks;

namespace BookStore.Application.Features.Books.Commands.CreateProduct
{
    public class CreateBookCommandValidator : AbstractValidator<CreateBookCommand>
    {
        private readonly IBookRepositoryAsync bookRepository;

        public CreateBookCommandValidator(IBookRepositoryAsync productRepository)
        {
            this.bookRepository = productRepository;

            RuleFor(p => p.Barcode)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                .MustAsync(IsUniqueBarcode).WithMessage("{PropertyName} already exists.");

            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

        }

        private async Task<bool> IsUniqueBarcode(string barcode, CancellationToken cancellationToken)
        {
            return await bookRepository.IsUniqueBarcodeAsync(barcode);
        }
    }
}
