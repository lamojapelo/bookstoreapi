﻿using AutoMapper;
using BookStore.Application.Interfaces.Repositories;
using BookStore.Application.Wrappers;
using BookStore.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BookStore.Application.Features.BasketItems.Commands.AddToBasket
{
    public class AddItemIntoBasketCommand : IRequest<Response<int>>
    {
        public int BookId { get; set; }
        public int Quantity { get; set; }
        public int UserId { get; set; }
    }

    public class CreateProductCommandHandler : IRequestHandler<AddItemIntoBasketCommand, Response<int>>
    {
        private readonly IBasketRepositoryAsync _basketRepository;
        private readonly IMapper _mapper;
        public CreateProductCommandHandler(IBasketRepositoryAsync basketRepository, IMapper mapper)
        {
            _basketRepository = basketRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(AddItemIntoBasketCommand request, CancellationToken cancellationToken)
        {
            var item = _mapper.Map<BasketItem>(request);
            await _basketRepository.AddAsync(item);
            return new Response<int>(item.Id);
        }
    }
}
