﻿using BookStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Application.Interfaces.Repositories
{
    public interface IBasketRepositoryAsync : IGenericRepositoryAsync<BasketItem>
    {
        //Task<BasketItem> AddItemintoBasketAsync(BasketItem basketItem);
        //Task<IList<BasketItem>> ChangeBasketItemQuantityAsync(int id, int quantity);
        //Task<IList<BasketItem>> ClearBasketAsync(int userId);
        //Task<IList<BasketItem>> DeleteBasketItemByIdAsync(int id);
        //Task<IList<BasketItem>> GetBasketItemsAsync(int userId);
    }
}
