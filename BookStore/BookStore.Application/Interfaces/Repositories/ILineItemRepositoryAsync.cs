﻿using BookStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Application.Interfaces.Repositories
{
    public interface ILineItemRepositoryAsync : IGenericRepositoryAsync<LineItem>
    {
    }
}
